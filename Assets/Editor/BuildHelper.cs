﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Unity.Build;

namespace Otsimo.Editor
{
    public class BuildHelper
    {
        [MenuItem("File / Build And Run MacOS Tiny")]
        public static void BuildForTinyMacOS()
        {
            BuildConfiguration bc = Unity.Build.BuildConfiguration.LoadAsset("Assets/Build/MacOS-DotNet.buildconfiguration");
            //var profile = bc.GetComponent<Unity.Build.DotsRuntime.DotsRuntimeBuildProfile>();
            //profile.Configuration = BuildType.Develop;
            // var wasm = bc.GetComponent<Unity.Build.Web.DotsRuntime.WasmOutputSettings>();
            var result = bc.Build();
            UnityEngine.Debug.LogFormat("BUILD::\n{0}", result.ToString());
            if (!result.Succeeded)
            {
                Debug.LogError("Failed to build project: " + result.Message);
            }
            else
            {
                bc.Run();
            }
        }

        [MenuItem("File / Build And Run Linux Tiny")]
        public static void BuildForTinyLinux()
        {
            BuildConfiguration bc = Unity.Build.BuildConfiguration.LoadAsset("Assets/Build/Linux-DotNet.buildconfiguration");
            //var profile = bc.GetComponent<Unity.Build.DotsRuntime.DotsRuntimeBuildProfile>();
            //profile.Configuration = BuildType.Develop;
            // var wasm = bc.GetComponent<Unity.Build.Web.DotsRuntime.WasmOutputSettings>();
            var result = bc.Build();
            UnityEngine.Debug.LogFormat("BUILD::\n{0}", result.ToString());
            if (!result.Succeeded)
            {
                Debug.LogError("Failed to build project: " + result.Message);
            }
            else
            {
                bc.Run();
            }
        }
    }
}
