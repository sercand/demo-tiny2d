﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Tiny;
using Unity.Tiny.Input;
using Unity.Transforms;
using Unity.U2D.Entities.Physics;


namespace Otsimo
{

    public class InputTesting : SystemBase
    {
        protected override void OnUpdate()
        {
             //var cmdBuffer = new EntityCommandBuffer(Allocator.TempJob);
             var input = World.GetExistingSystem<InputSystem>();
             var physicsWorld = World.GetExistingSystem<PhysicsWorldSystem>().PhysicsWorld;
             if (InputUtil.GetInputUp(input))
             {
                 var pos = CameraUtil.ScreenPointToWorldPoint(World, InputUtil.GetInputPosition(input));
                 var pi = new OverlapPointInput()
                 {
                     Position = pos,
                     Filter = CollisionFilter.Default,
                 };
                 if (physicsWorld.OverlapPoint(pi, out var overlapPointHit))
                 {
                     var body = physicsWorld.AllBodies[overlapPointHit.PhysicsBodyIndex];
                     var entity = body.Entity;
                     Entities
                     .ForEach((Entity e, ref AnimationState state, in TranslatePingPong tr) =>
                     {
                         if (e == entity)
                         {
                             state.Forward = !state.Forward;
                             state.Duration = tr.Duration - state.Duration;
                         }
                     }).Schedule();
                 }
                 else
                 {
                     Entity eh0 = Entity.Null;
                     Entity off = Entity.Null;
                     Entities.ForEach((Entity e, in HearthContainer hc) =>
                     {
                         eh0 = hc.Hearth0;
                         off = hc.SpriteOff;
                     }).Run();
                     if (eh0 != Entity.Null && off != Entity.Null)
                     {
                         var hc0 = EntityManager.GetComponentData<SpriteRenderer>(eh0);
                         hc0.Sprite = off;
                         EntityManager.SetComponentData(eh0, hc0);
                     }
                 }
             }
             // cmdBuffer.Playback(EntityManager);
             //  cmdBuffer.Dispose();*/

        }
    }
}