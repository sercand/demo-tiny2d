﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace Otsimo
{

    public class SampleAnimationSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var cmdBuffer = new EntityCommandBuffer(Allocator.TempJob);
            //UnityEngine.Debug.Log("On SampleAnimationSystem Update");

            var deltaTime = Time.DeltaTime; // 0.016 sec

            Entities
            .WithAll<TranslatePingPong, AnimationState>()
            .ForEach((Entity e, ref Translation translation, ref AnimationState state, in TranslatePingPong t) =>
            {
                var dt = t.Delta * deltaTime;
                state.Duration += deltaTime;
                if (state.Duration > t.Duration)
                {
                    state.Duration = 0;
                    state.Forward = !state.Forward;
                }
                if (state.Forward)
                {
                    translation.Value = translation.Value + dt;
                }
                else
                {
                    translation.Value = translation.Value - dt;
                }
            }).Schedule();


            Entities
            .WithAll<RotationSpeed>()
            .ForEach((ref Rotation rotation, in RotationSpeed rotationSpeed) =>
            {
                var rotationAmount = quaternion.RotateZ(rotationSpeed.Value * deltaTime);
                rotation.Value = math.mul(rotation.Value, rotationAmount);
            }).Schedule();


            Entities.WithAll<TranslatePingPong>()
                .WithNone<AnimationState>()
                .ForEach((Entity e) =>
                {
                    cmdBuffer.AddComponent(e, new AnimationState
                    {
                        Duration = 0,
                        Forward = true,
                    });
                    UnityEngine.Debug.Log("Add SampleAnimationSystem component");
                })
                .Run();
            cmdBuffer.Playback(EntityManager);
            cmdBuffer.Dispose();
        }
    }

}