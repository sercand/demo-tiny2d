﻿using Unity.Entities;
using Unity.Mathematics;

namespace Otsimo
{
    [GenerateAuthoringComponent]
    public struct TranslatePingPong : IComponentData
    {
        public float3 Delta;
        public float Duration;
    }
}
