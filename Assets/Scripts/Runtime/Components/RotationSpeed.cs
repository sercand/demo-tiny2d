﻿using Unity.Entities;

namespace Otsimo
{
    [GenerateAuthoringComponent]
    public struct RotationSpeed : IComponentData
    {
        public float Value;
    }
}
